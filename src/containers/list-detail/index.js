import React, { Component } from "react";
import { View, Text, TouchableOpacity, TouchableHighlight } from "react-native";
import { styles } from "./styles";
import ViewWithHeader from "../../components/view-with-header";
import CustomHeader from "../../components/custom-header";
import { titleStyle, headerTitleRight } from "../../common/global";
import { connect } from "react-redux";
import ShopAction from "../../actions/shop";
import { getShopField } from "../../selector/shop";
import Loader from "../../components/loader";
import { Button } from "react-native-elements";
import Snackbar from "react-native-snackbar";
import { Router } from "../../navigation/route";

class index extends Component {
	static navigationOptions = {
		header: null
	};
	state = {
		name: "",
		note: "",
		ingredients: [],
		id: ""
	};
	componentDidMount() {
		const { name, note, ingredients, id } = this.props.navigation.getParam(
			"item"
		);
		this.setState({ name, note, ingredients, id });
	}
	componentWillUnmount() {
		const { name, note, ingredients, id } = this.state;
		this.props.updateList({ name, note, ingredients, id });
		this.props.getList(this.props.user.uid);
	}
	edit = () => {
		const {
			changeFormField,
			navigation: { replace }
		} = this.props;
		const { name, note, ingredients, id } = this.state;
		changeFormField("id", id);
		changeFormField("name", name);
		changeFormField("note", note);
		changeFormField("ingredients", ingredients);
		changeFormField("currentId", ingredients.length - 1);

		replace(Router.AddShopList, { type: "update" });
	};
	checkBought = index => {
		const { ingredients } = this.state;
		let newArray = [...ingredients];
		newArray[index].checked = !ingredients[index].checked;
		this.setState({ ingredients: newArray });
	};
	renderItem = ({ item, index }) => {
		const checked = item.checked;
		return (
			<TouchableHighlight
				underlayColor={"#eee"}
				onPress={() => this.checkBought(index)}
			>
				<View
					style={checked ? styles.itemContainerHighlight : styles.itemContainer}
				>
					<View style={styles.row}>
						<Text
							style={
								checked ? styles.itemQuantityHighlight : styles.itemQuantity
							}
						>
							{item.quantity}
						</Text>
						<Text
							style={
								checked ? styles.itemIngredientHighlight : styles.itemIngredient
							}
						>
							{item.ingredient}
						</Text>
					</View>
				</View>
			</TouchableHighlight>
		);
	};
	renderListHeader = () => {
		const { name, note } = this.state;
		return (
			<View style={styles.listHeader}>
				<Text style={titleStyle}>List name</Text>
				<Text style={styles.text}>{name}</Text>
				<Text style={titleStyle}>List note</Text>
				<Text style={styles.text}>{note}</Text>
				<Text style={titleStyle}>Ingredients</Text>
				<Text style={styles.info}>
					Touch on ingredient to check it was bought
				</Text>
			</View>
		);
	};
	render() {
		const { ingredients } = this.state;
		return (
			<ViewWithHeader
				contentContainerStyle={styles.container}
				showsVerticalScrollIndicator={false}
				header={
					<CustomHeader
						navigation={this.props.navigation}
						title={"List Details"}
						headerRight={
							<TouchableOpacity onPress={this.edit}>
								<Text style={headerTitleRight}>Edit</Text>
							</TouchableOpacity>
						}
					/>
				}
				data={ingredients}
				extraData={ingredients}
				keyExtractor={item => item.id.toString()}
				renderItem={this.renderItem}
				ListHeaderComponent={this.renderListHeader}
			/>
		);
	}
}

const mapStateToProps = state => ({
	form: getShopField(state, "form"),
	user: state.auth.user
});

const mapDispatchToProps = {
	changeFormField: ShopAction.changeFormField,
	getList: ShopAction.getList,
	pushList: ShopAction.pushList,
	updateList: ShopAction.updateList,
	resetForm: ShopAction.resetForm
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
