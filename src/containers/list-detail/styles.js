import { ScaledSheet } from "react-native-size-matters";
import { Color, Font } from "../../common";

export const styles = ScaledSheet.create({
	container: {
		paddingTop: 56,
		backgroundColor: "#fff",
		marginHorizontal: 8,
		overflow: "hidden"
	},
	itemContainer: {
		backgroundColor: "#f9f9f9",
		paddingHorizontal: "16@ms",
		paddingVertical: "12@vs",
		borderRadius: 4,
		marginBottom: 8
	},
	itemContainerHighlight: {
		backgroundColor: Color.lightGreen,
		paddingHorizontal: "16@ms",
		paddingVertical: "12@vs",
		borderRadius: 4,
		marginBottom: 8
	},
	row: {
		flexDirection: "row",
		textAlign: "center"
	},
	itemQuantity: {
		fontSize: 20,
		color: Color.orange
	},
	itemQuantityHighlight: {
		fontSize: 20,
		color: "#fff"
	},
	itemIngredient: {
		fontSize: 20,
		flex: 1,
		textAlign: "center"
	},
	itemIngredientHighlight: {
		fontSize: 20,
		flex: 1,
		textAlign: "center",
		color: "#fff"
	},
	text: {
		fontSize: 18,
		textAlign: "center",
		fontFamily: Font.Family.OpenSansLightItalic,
		backgroundColor: "#fafafa"
	},
	listHeader: {
		marginBottom: 16
	},
	info: {
		fontSize: 12,
		fontFamily: Font.Family.OpenSansLightItalic,
		textAlign: "center"
	}
});
