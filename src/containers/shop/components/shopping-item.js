import React from "react";
import { TouchableHighlight, View, Text } from "react-native";
import { convertTimeToString } from "../../../common/util";
import { ScaledSheet } from "react-native-size-matters";
import { Font, Color } from "../../../common";

export default ({ data, onPress }) => {
	const { name, note, ingredients, createdAt } = data;
	const numBoughtIngredients = ingredients.filter(e => e.checked === true)
		.length;
	const numIngredients = ingredients.length;
	return (
		<TouchableHighlight
			style={styles.container}
			onPress={onPress}
			underlayColor={"#eee"}
		>
			<View>
				<Text style={styles.timeStamp}>
					{convertTimeToString(createdAt, "DD-MM-YYYY hh:mm")}
				</Text>
				<View style={styles.row}>
					<View style={styles.content}>
						<Text style={styles.name}>{name}</Text>
						<Text style={styles.note}>{note}</Text>
					</View>
					<Text style={styles.num}>
						{numBoughtIngredients}/{numIngredients}
					</Text>
				</View>
			</View>
		</TouchableHighlight>
	);
};

const styles = ScaledSheet.create({
	container: {
		padding: "8@vs",
		paddingHorizontal: "16@ms",
		backgroundColor: "#fff",
		borderRadius: "8@ms",
		marginBottom: "8@vs",
		overflow: "hidden",
		elevation: 1
	},
	row: {
		flexDirection: "row",
		alignItems: "center"
	},
	content: {
		flex: 1,
		paddingRight: "8@ms"
	},
	timeStamp: {
		fontSize: 14,
		color: "#ccc",
		fontFamily: Font.Family.OpenSansLightItalic
	},
	name: {
		fontSize: 18,
		color: "#000",
		fontFamily: Font.Family.OpenSansRegular
	},
	note: {
		fontSize: 16,
		color: "#999",
		fontFamily: Font.Family.OpenSansLightItalic
	},
	num: {
		color: Color.orange
	}
});
