import React, { Component } from "react";
import {
	View,
	Text,
	TextInput,
	TouchableOpacity,
	TouchableHighlight
} from "react-native";
import { styles } from "./styles";
import ViewWithHeader from "../../components/view-with-header";
import CustomHeader from "../../components/custom-header";
import { titleStyle } from "../../common/global";
import Modal, {
	ScaleAnimation,
	ModalContent,
	ModalTitle,
	ModalFooter,
	ModalButton
} from "react-native-modals";
import Swipeable from "react-native-swipeable";
import { connect } from "react-redux";
import ShopAction from "../../actions/shop";
import { getShopField } from "../../selector/shop";
import Loader from "../../components/loader";
import { Button } from "react-native-elements";
import Snackbar from "react-native-snackbar";

const defaultCurrent = {
	id: 0,
	quantity: "",
	ingredient: "",
	checked: false
};

class index extends Component {
	static navigationOptions = {
		header: null
	};
	state = {
		type: this.props.navigation.getParam("type", "create"),
		visible: false,
		current: defaultCurrent,
		currentId: this.props.form.currentId,
		currentError: ""
	};

	toggleVisible = () =>
		this.setState(prev => ({
			visible: !prev.visible
		}));

	addIngredient = () => {
		const { ingredient, quantity } = this.state.current;
		const {
			form: { ingredients, currentId },
			changeFormField
		} = this.props;
		if (ingredient !== "" && quantity !== "") {
			let newCurrent = { ...this.state.current };
			newCurrent.id = currentId + 1;
			this.setState({ current: defaultCurrent, currentError: "" }, () => {
				changeFormField("currentId", currentId + 1);
				changeFormField("ingredients", [...ingredients, newCurrent]);
				this.toggleVisible();
			});
		} else {
			this.setState({ currentError: "Not a valid field" });
		}
	};

	renderListHeader = () => {
		const {
			changeFormField,
			form: { name, note }
		} = this.props;
		const { quantity, ingredient } = this.state.current;
		return (
			<View style={styles.listHeader}>
				<Text style={titleStyle}>List name</Text>
				<TextInput
					value={name}
					onChangeText={name => changeFormField("name", name)}
				/>

				<Text style={titleStyle}>List note</Text>
				<TextInput
					multiline
					value={note}
					onChangeText={note => changeFormField("note", note)}
				/>

				<Text style={titleStyle}>Items</Text>
				<TouchableOpacity onPress={this.toggleVisible}>
					<Text style={styles.add}>Add Ingredient</Text>
				</TouchableOpacity>
				<Modal
					visible={this.state.visible}
					onTouchOutside={this.toggleVisible}
					modalTitle={
						<ModalTitle
							textStyle={styles.modalTitle}
							title={"Add Ingredient"}
						/>
					}
					modalAnimation={
						new ScaleAnimation({
							initialValue: 0, // optional
							useNativeDriver: true // optional
						})
					}
					footer={
						<ModalFooter>
							<ModalButton
								textStyle={styles.modalButton}
								text={"CANCEL"}
								onPress={this.toggleVisible}
							/>
							<ModalButton
								textStyle={styles.modalButton}
								text={"OK"}
								onPress={this.addIngredient}
							/>
						</ModalFooter>
					}
				>
					<ModalContent style={styles.modalContent}>
						<Text style={titleStyle}>Ingredient</Text>
						<TextInput
							defaultValue={ingredient}
							onChangeText={ingredient => {
								let temp = { ...this.state.current };
								temp.ingredient = ingredient;
								this.setState({ current: temp });
							}}
						/>
						<Text style={titleStyle}>Quantity</Text>
						<TextInput
							defaultValue={quantity}
							onChangeText={quantity => {
								let temp = { ...this.state.current };
								temp.quantity = quantity;
								this.setState({ current: temp });
							}}
							keyboardType={"numeric"}
						/>
					</ModalContent>
				</Modal>

				<Loader visible={this.props.form.isLoading} />
			</View>
		);
	};

	onSave = () => {
		const {
			user,
			pushList,
			updateList,
			resetForm,
			getList,
			form: { name, note, ingredients, id },
			navigation: { goBack }
		} = this.props;
		const { type } = this.state;
		let item = { name, note, ingredients };
		if (name !== "" && note !== "" && ingredients.length > 0) {
			type === "create"
				? pushList(user.uid, item)
				: updateList({ name, note, ingredients, id });
			resetForm();
			getList(user.uid);
			goBack();
		} else {
			Snackbar.show({
				title: "All fields are required"
			});
		}
	};

	renderListFooter = () => {
		return (
			<Button
				onPress={this.onSave}
				title={"SAVE"}
				titleStyle={styles.btnTitle}
				buttonStyle={styles.btnStyle}
				containerStyle={styles.btnContainer}
			/>
		);
	};

	onDeleteItem = item => {
		const {
			form: { ingredients },
			changeFormField
		} = this.props;
		changeFormField(
			"ingredients",
			ingredients.filter(e => e !== item)
		);
	};

	renderItem = ({ item }) => (
		<Item data={item} onDelete={() => this.onDeleteItem(item)} />
	);

	render() {
		const { error, ingredients } = this.props.form;
		return (
			<ViewWithHeader
				contentContainerStyle={styles.container}
				showsVerticalScrollIndicator={false}
				header={
					<CustomHeader
						navigation={this.props.navigation}
						title={"New Shopping List"}
					/>
				}
				data={ingredients}
				extraData={ingredients}
				keyExtractor={item => item.id.toString()}
				renderItem={this.renderItem}
				ListHeaderComponent={this.renderListHeader}
				ListFooterComponent={this.renderListFooter}
			/>
		);
	}
}

const mapStateToProps = state => ({
	form: getShopField(state, "form"),
	user: state.auth.user
});

const mapDispatchToProps = {
	changeFormField: ShopAction.changeFormField,
	getList: ShopAction.getList,
	pushList: ShopAction.pushList,
	updateList: ShopAction.updateList,
	resetForm: ShopAction.resetForm
};

export default connect(mapStateToProps, mapDispatchToProps)(index);

const Item = ({ data, onPress, onDelete }) => (
	<Swipeable
		leftContent={<Text style={styles.leftContent}>Delete</Text>}
		onLeftActionRelease={onDelete}
		style={{ marginBottom: 8 }}
	>
		<TouchableHighlight
			onPress={onPress}
			underlayColor={"#eee"}
			style={styles.itemContainer}
		>
			<View style={styles.row}>
				<Text style={styles.itemQuantity}>{data.quantity}</Text>
				<Text style={styles.itemIngredient}>{data.ingredient}</Text>
			</View>
		</TouchableHighlight>
	</Swipeable>
);
