import { ScaledSheet } from "react-native-size-matters";
import { Font, Color } from "../../common";

export default ScaledSheet.create({
	container: { padding: "8@ms", paddingTop: 56 },
	btnTitle: {
		fontSize: Font.Size.header,
		fontFamily: Font.Family.OpenSansLight
	},
	btnStyle: {
		backgroundColor: Color.green,
		width: "300@ms0.3"
	},
	btnContainer: {
		alignItems: "center",
		marginVertical: "16@vs"
	},
	row: {
		flexDirection: "row",
		alignItems: "center"
	},
	name: {
		paddingHorizontal: "8@ms",
		flex: 1
	},
	header: {
		paddingVertical: "16@vs",
		marginVertical: "8@vs",
		backgroundColor: "#fff",
		borderRadius: "8@ms"
	},
	info: {
		textAlign: "center",
		fontSize: 20,
		fontFamily: Font.Family.OpenSansLightItalic
	}
});
