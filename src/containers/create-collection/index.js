import React, { Component } from "react";
import ViewWithHeader from "../../components/view-with-header";
import styles from "./styles";
import { TextInput, View, Image, TouchableHighlight } from "react-native";
import { inputStyle, renderHeader, titleStyle } from "../../common/global";
import { Text } from "react-native-paper";
import { Button } from "react-native-elements";
import CustomHeader from "../../components/custom-header";
import { connect } from "react-redux";
import { Router } from "../../navigation/route";
import PlanAction from "../../actions/plan";

class index extends Component {
	static navigationOptions = {
		header: null
	};
	state = {
		type: this.props.navigation.getParam("type", "default"),
		key: this.props.navigation.getParam("key")
	};
	componentDidMount() {
		const { type, key } = this.state;
		console.log("====================================");
		console.log("My cookbook", type, key);
		console.log("====================================");
	}

	renderListHeader = () => {
		if (this.state.type === "select") {
			return (
				<View style={styles.header}>
					<Text style={styles.info}>Touch on recipe to choose</Text>
				</View>
			);
		}
		return null;
	};

	renderItem = ({ item, index }) => {
		return (
			<TouchableHighlight
				underlayColor={"#eee"}
				onPress={() => this.onPress(index)}
			>
				<View style={styles.row}>
					<Image
						resizeMode={"contain"}
						resizeMethod={"resize"}
						style={{
							width: 100,
							height: 100,
							borderRadius: 12
						}}
						source={{ uri: item.image }}
					/>
					<Text style={styles.name}>{item.name}</Text>
				</View>
			</TouchableHighlight>
		);
	};
	onPress = index => {
		if (this.state.type === "select") {
			const {
				user: { cookBook },
				pushItem,
				navigation: { goBack }
			} = this.props;
			pushItem(cookBook[index], this.state.key);
			goBack();
		} else {
			this.props.navigation.navigate(Router.CookDetail, { index });
		}
	};
	render() {
		const {
			user: { cookBook }
		} = this.props;
		return (
			<ViewWithHeader
				contentContainerStyle={styles.container}
				showsVerticalScrollIndicator={false}
				header={
					<CustomHeader
						navigation={this.props.navigation}
						title={"My Cookbook"}
					/>
				}
				data={cookBook}
				extraData={cookBook}
				keyExtractor={(item, index) => index.toString()}
				renderItem={this.renderItem}
				ListHeaderComponent={this.renderListHeader}
				ListFooterComponent={this.renderListFooter}
			/>
		);
	}
}

const mapStateToProps = state => ({
	user: state.auth.user,
	isFetching: state.auth.isFetching,
	error: state.auth.error
});

const mapDispatchToProps = {
	pushItem: PlanAction.pushItem
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
