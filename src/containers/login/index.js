import React, { Component } from "react";
import {
	View,
	Text,
	ImageBackground,
	ScrollView,
	TouchableOpacity,
	Image
} from "react-native";
import { styles } from "./styles";
import CustomInput from "../../components/custom-input";
import background from "../../assets/images/background.jpeg";
import { Router } from "../../navigation/route";
import { Button } from "react-native-elements";
import { connect } from "react-redux";
import AuthAction from "../../actions/auth";
import { getField } from "../../selector/auth";
import logo from "../../assets/images/logo.png";
import Snackbar from "react-native-snackbar";
import { setUser } from "../../config/storage";

class index extends Component {
	state = {};
	login = () => {
		const {
			isFetching,
			error,
			navigation,
			email,
			password,
			emailError,
			passwordError
		} = this.props;

		console.log(emailError, passwordError);

		if (emailError === "" && passwordError === "") {
			this.props.login(email, password);
			console.log(isFetching, error);
		}
	};
	async componentWillReceiveProps(nextProps) {
		console.log("=======", nextProps.logoutState, nextProps.user);

		if (nextProps.logoutState === false && nextProps.user) {
			Snackbar.show({
				title: "Login Successfully"
			});
			await setUser(nextProps.user);
			this.props.navigation.navigate(Router.Main);
		}
		if (nextProps.error) {
			Snackbar.show({
				title: nextProps.error
			});
		}
	}
	render() {
		const {
			isFetching,
			email,
			password,
			emailError,
			passwordError,
			checkEmail,
			checkPassword,
			changeFormField
		} = this.props;
		return (
			<ImageBackground
				source={background}
				style={styles.container}
				blurRadius={0.5}
			>
				<ScrollView showsVerticalScrollIndicator={false}>
					<Image
						source={logo}
						resizeMode={"contain"}
						resizeMethod={"resize"}
						style={styles.logo}
					/>
					<View style={styles.form}>
						<Text style={styles.title}>Welcome back</Text>
						<CustomInput
							label={"Email"}
							value={email}
							onChangeText={email => {
								checkEmail(email);
								changeFormField("email", email);
							}}
							errorText={emailError}
							keyboardType={"email-address"}
						/>
						<CustomInput
							label={"Password"}
							value={password}
							onChangeText={password => {
								checkPassword(password);
								changeFormField("password", password);
							}}
							errorText={passwordError}
							secureTextEntry={true}
						/>
						<View style={styles.insWrapper}>
							<Text style={styles.instruction}>Don't have an account,</Text>
							<TouchableOpacity
								onPress={() => this.props.navigation.navigate(Router.Register)}
							>
								<Text style={styles.highlight}>Register</Text>
							</TouchableOpacity>
						</View>
					</View>
					<Button
						title={"Login"}
						titleStyle={styles.createTitle}
						buttonStyle={styles.createButton}
						containerStyle={{ alignSelf: "center" }}
						onPress={this.login}
						loading={this.props.isFetching}
					/>
				</ScrollView>
			</ImageBackground>
		);
	}
}

const mapStateToProps = state => ({
	email: getField(state, "email"),
	emailError: getField(state, "emailError"),
	password: getField(state, "password"),
	passwordError: getField(state, "passwordError"),
	isFetching: state.auth.isFetching,
	error: state.auth.error,
	user: state.auth.user,
	logoutState: state.auth.logoutState
});

const mapDispatchToProps = {
	changeFormField: AuthAction.changeFormField,
	login: AuthAction.login,
	checkEmail: AuthAction.checkEmail,
	checkPassword: AuthAction.checkPassword
};
export default connect(mapStateToProps, mapDispatchToProps)(index);
