import React, { Component } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	TouchableHighlight,
	Image,
	StyleSheet
} from "react-native";
import { styles } from "./styles";
import { Button, Avatar, Icon } from "react-native-elements";
import { Router } from "../../navigation/route";
import { connect } from "react-redux";
import AuthAction from "../../actions/auth";
import UserAction from "../../actions/user";
import { setUser } from "../../config/storage";
import CustomView from "../../components/custom-view";
import Modal, {
	ScaleAnimation,
	ModalContent,
	ModalTitle,
	ModalFooter,
	ModalButton
} from "react-native-modals";
import ImagePicker from "react-native-image-picker";
import Snackbar from "react-native-snackbar";

const options = {
	title: "Select Avatar",
	mediaType: "photo",
	quality: 0.3,
	storageOptions: {
		skipBackup: true,
		path: "images"
	}
};

class index extends Component {
	state = {
		visible: false
	};
	signOut = () => {
		this.setState({ visible: false });
		setTimeout(async () => {
			await setUser(null);
			this.props.logout();
		}, 200);
	};
	editPhoto = () => {
		ImagePicker.showImagePicker(options, response => {
			if (response.didCancel) {
				console.log("User cancelled image picker");
			} else if (response.error) {
				console.log("ImagePicker Error: ", response.error);
			} else if (response.customButton) {
				console.log("User tapped custom button: ", response.customButton);
			} else {
				this.props
					.uploadAvatar(this.props.user, response.path.toString())
					.then(state => {
						if (state === "success") {
							Snackbar.show({
								title: "Update avatar successfully"
							});
						} else {
							Snackbar.show({
								title: "Something went wrong, please try later"
							});
						}
					});
			}
		});
	};
	componentWillReceiveProps(nextProps) {
		console.log("profile out state", nextProps.logoutState);

		if (nextProps.logoutState === true) {
			this.props.navigation.navigate(Router.Splash);
		}
	}

	toggleVisible = () => {
		this.setState({ visible: !this.state.visible });
	};
	render() {
		const {
			user: { displayName, email, photoURL, cookBook },
			navigation: { navigate }
		} = this.props;

		return (
			<CustomView containerStyle={styles.container}>
				<View style={[styles.row, styles.header]}>
					<Avatar
						showEditButton
						rounded
						size={"large"}
						source={{ uri: photoURL }}
						onPress={this.editPhoto}
					/>
					<View style={styles.headerContent}>
						<Text style={styles.name}>{displayName}</Text>
						<Text style={styles.email}>{email}</Text>
					</View>
					<TouchableOpacity onPress={this.toggleVisible}>
						<Icon
							name={"logout"}
							type={"antdesign"}
							size={24}
							color={"#999"}
							containerStyle={styles.logout}
						/>
					</TouchableOpacity>
				</View>

				<TouchableHighlight
					underlayColor={"#eee"}
					onPress={() => navigate(Router.CreateCollection)}
				>
					<View>
						<Text style={styles.title}>
							My Cookbook ({cookBook ? cookBook.length : 0})
						</Text>
						<Text style={styles.info}>Touch to view details</Text>
					</View>
				</TouchableHighlight>

				<Modal
					visible={this.state.visible}
					onTouchOutside={this.toggleVisible}
					modalTitle={
						<ModalTitle textStyle={styles.modalTitle} title={"LOG OUT"} />
					}
					modalAnimation={
						new ScaleAnimation({
							initialValue: 0, // optional
							useNativeDriver: true // optional
						})
					}
					footer={
						<ModalFooter>
							<ModalButton
								textStyle={styles.modalButton}
								text={"CANCEL"}
								onPress={this.toggleVisible}
							/>
							<ModalButton
								textStyle={styles.modalButton}
								text={"OK"}
								onPress={this.signOut}
							/>
						</ModalFooter>
					}
				>
					<ModalContent style={styles.modalContent}>
						<Text style={styles.modalText}>Are you sure to logout ?</Text>
					</ModalContent>
				</Modal>
			</CustomView>
		);
	}
}

const mapStateToProps = state => ({
	isFetching: state.auth.isFetching,
	error: state.auth.error,
	user: state.auth.user,
	logoutState: state.auth.logoutState
});

const mapDispatchToProps = {
	logout: AuthAction.logout,
	uploadAvatar: UserAction.uploadAvatar
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
