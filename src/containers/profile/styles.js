import { ScaledSheet } from "react-native-size-matters";
import { Font, Color } from "../../common";

export const styles = ScaledSheet.create({
	container: { flex: 1, padding: 16 },
	row: {
		flexDirection: "row",
		alignItems: "center"
	},
	header: {
		padding: "16@ms",
		backgroundColor: "#fff",
		borderRadius: 100,
		elevation: 1
	},
	headerContent: {
		flex: 1,
		paddingHorizontal: "8@ms"
	},
	name: {
		fontSize: 20,
		fontFamily: Font.Family.OpenSansRegular,
		letterSpacing: 1
	},
	email: {
		fontSize: 14,
		fontFamily: Font.Family.OpenSansLightItalic
	},
	modalContent: { width: "280@ms0.2" },
	modalButton: {
		fontSize: 16,
		fontFamily: Font.Family.OpenSansLight,
		color: Color.lightGreen
	},
	modalTitle: {
		fontSize: 16
	},
	modalText: {
		marginTop: 12,
		textAlign: "center"
	},
	title: {
		fontSize: 24,
		letterSpacing: 1,
		fontFamily: Font.Family.OpenSansSemiBold,
		marginVertical: 12,
		textAlign: "center"
	},
	info: {
		textAlign: "center",
		color: Color.green
	}
});
