import React, { Component } from "react";
import {
	View,
	StyleSheet,
	FlatList,
	TouchableOpacity,
	ActivityIndicator,
	StatusBar
} from "react-native";
import { styles } from "./styles";
import Carousel from "react-native-snap-carousel";
import SliderEntry, {
	sliderWidth,
	itemWidth
} from "../../components/slider-entry";
import { connect } from "react-redux";
import { getRecipeField, getEntireRecipeState } from "../../selector/recipe";
import {
	getCategoryField,
	getEntireCategoryState
} from "../../selector/category";
import RecipeAction from "../../actions/recipe";
import CategoryAction from "../../actions/category";
import Tabs from "../../components/tabs";
import { Icon } from "react-native-elements";
import { Color } from "../../common";
import { Router } from "../../navigation/route";
import Loader from "../../components/loader";
import Snackbar from "react-native-snackbar";

class index extends Component {
	constructor(props) {
		super(props);
		this.state = {
			index: 0
		};
	}
	limit = 9;

	componentDidMount() {
		const { getRecipes, getCategories } = this.props;
		getRecipes("0");
		getCategories();
	}

	// componentWillUpdate(nextProps, nextState) {
	// 	const { recipeData, getRecipes } = this.props;
	// 	if (nextState.index === this.limit) {
	// 		const lastId = recipeData.pop().id;
	// 		getRecipes(initCate, lastId);
	// 	}
	// }

	// shouldComponentUpdate(nextProps, nextState) {
	// 	const { recipeData } = this.props;
	// 	this.limit = recipeData.length > 0 ? recipeData.length - 1 : 9;
	// 	// console.log("nextState.index :", nextState.index);
	// 	// console.log("this.limit :", this.limit);
	// 	if (nextState.index !== this.limit && nextProps === this.props) {
	// 		return false;
	// 	}
	// 	return true;
	// }

	toggleLike = (item, status) => {
		this.props
			.setUserLike(item, status)
			.then(() => {
				Snackbar.show({
					title: status ? "Liked ! Check your cookbook" : "Disliked!"
				});
			})
			.catch(error => {
				Snackbar.show({
					title: "Can not like right now, try later!" + error
				});
			});
	};

	_renderItemWithParallax({ item, index }, parallaxProps) {
		const {
			user: { uid }
		} = this.props;

		let status = item[uid] ? false : true;

		return (
			<SliderEntry
				onPress={() =>
					this.props.navigation.navigate(Router.RecipeDetails, {
						item
					})
				}
				uid={this.props.user.uid}
				data={item}
				parallax={true}
				parallaxProps={parallaxProps}
				onSave={() => this.toggleLike(item, status)}
			/>
		);
	}

	renderListFooter = () => {
		if (!this.props.isRecipeLoading) return null;
		return (
			<View style={styles.footer}>
				<ActivityIndicator animating size={"small"} />
			</View>
		);
	};

	filter = (data, category) => {
		console.log(data.length);

		return data.filter(e => e[category] === true);
	};

	renderList() {
		return (
			<View style={styles.exampleContainer}>
				<TouchableOpacity
					style={{ alignSelf: "flex-end" }}
					onPress={() => this.props.navigation.navigate(Router.Search)}
				>
					<Icon
						name={"search1"}
						type={"antdesign"}
						color={Color.green}
						size={24}
						containerStyle={{ padding: 16 }}
					/>
				</TouchableOpacity>
				<Tabs
					tabs={this.props.categoryData}
					onTabPress={category => this.props.changeCategory(category)}
				/>
				<Carousel
					// ListFooterComponent={this.renderListFooter}
					// onSnapToItem={index => this.setState({ index })}
					removeClippedSubviews={true}
					extraData={this.props.recipeData}
					ref={c => (this._slider1Ref = c)}
					data={this.filter(this.props.recipeData, this.props.category)}
					renderItem={this._renderItemWithParallax.bind(this)}
					sliderWidth={sliderWidth}
					itemWidth={itemWidth}
					hasParallaxImages={true}
					firstItem={1}
					inactiveSlideScale={0.94}
					inactiveSlideOpacity={0.5}
					containerCustomStyle={styles.slider}
					contentContainerCustomStyle={styles.sliderContentContainer}
					loop={false}
					loopClonesPerSide={2}
					autoplay={false}
					autoplayDelay={1000}
					autoplayInterval={3000}
				/>
			</View>
		);
	}

	render() {
		const {
			isRecipeLoading,
			recipeError,
			recipeData,
			isCategoryLoading,
			categoryError,
			categoryData
		} = this.props;
		console.log("=========================================explore");

		// recipeData.map(e => {
		// 	console.log(e.name);
		// });
		return (
			<View style={styles.container}>
				{categoryData.length > 0 && this.renderList()}
				<Loader visible={isRecipeLoading} />
			</View>
		);
	}
}

const mapStateToProps = state => ({
	isRecipeLoading: getRecipeField(state, "isLoading"),
	recipeError: getRecipeField(state, "error"),
	recipeData: getRecipeField(state, "data"),
	category: state.recipe.category,
	recipe: getEntireRecipeState(state),

	isCategoryLoading: getCategoryField(state, "isLoading"),
	categoryError: getCategoryField(state, "error"),
	categoryData: getCategoryField(state, "data"),
	categories: getEntireCategoryState(state),

	user: state.auth.user
});

const mapDispatchToProps = {
	getRecipes: RecipeAction.getRecipes,
	setUserLike: RecipeAction.setUserLike,
	changeCategory: RecipeAction.changeCategory,
	getCategories: CategoryAction.getCategories
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
