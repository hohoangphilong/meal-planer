import { ScaledSheet } from "react-native-size-matters";
import { Color, Font } from "../../common";
import { StyleSheet } from "react-native";

export default styles = ScaledSheet.create({
	container: {
		backgroundColor: "#fff"
	},
	inputStyles: {
		fontSize: 16,
		paddingVertical: 0,
		backgroundColor: "#f5f5f5",
		color: "grey"
	},
	inputContainerStyles: { backgroundColor: "#f5f5f5" },
	containerStyles: { backgroundColor: Color.lightGrey, marginTop: "8@vs" },
	searchItem: {
		paddingBottom: "4@vs",
		paddingHorizontal: "8@ms",
		fontSize: Font.Size.header,
		fontFamily: Font.Family.OpenSansLightItalic
	},
	row: {
		flexDirection: "row",
		alignItems: "center"
	}
});
