import React, { Component } from "react";
import PropTypes from "prop-types";
import { SearchBar } from "react-native-elements";
import CustomView from "../../components/custom-view";
import { InstantSearch } from "react-instantsearch/native";
import {
	connectSearchBox,
	connectInfiniteHits
} from "react-instantsearch/connectors";
import Config from "../../config/config";
import { FlatList, Text, TouchableHighlight, Image, View } from "react-native";
import styles from "./styles";
import { Router } from "../../navigation/route";

class Search extends Component {
	static propTypes = {
		refine: PropTypes.func.isRequired,
		currentRefinement: PropTypes.string
	};
	render() {
		return (
			<SearchBar
				placeholder={"Search..."}
				round
				platform={"ios"}
				showLoadingIcon={true}
				onChangeText={text => this.props.refine(text)}
				value={this.props.currentRefinement}
				inputStyle={styles.inputStyle}
				inputContainerStyle={styles.inputContainerStyle}
				containerStyle={styles.containerStyles}
			/>
		);
	}
}

const ConnectedSearchBar = connectSearchBox(Search);

class Hits extends Component {
	render() {
		console.log(this.props.navigation);
		const hits =
			this.props.hits.length > 0 ? (
				<FlatList
					data={this.props.hits}
					renderItem={this.renderItem}
					keyExtractor={item => item.objectID}
				/>
			) : null;
		return hits;
	}
	renderItem = ({ item }) => {
		return (
			<TouchableHighlight
				onPress={() => {
					this.props.navigation.replace(Router.RecipeDetails, { item });
				}}
			>
				<View style={styles.row}>
					<Image
						resizeMethod={"resize"}
						resizeMode={"contain"}
						source={{ uri: item.image }}
						style={{ width: 40, height: 40, borderRadius: 8 }}
					/>
					<Text style={styles.searchItem}>{item.name}</Text>
				</View>
			</TouchableHighlight>
		);
	};
}
Hits.propTypes = {
	hits: PropTypes.array.isRequired,
	refine: PropTypes.func.isRequired,
	hasMore: PropTypes.bool.isRequired
};
const ConnectedHits = connectInfiniteHits(Hits);
export default class index extends Component {
	static navigationOptions = {
		header: null
	};
	state = {};
	render() {
		return (
			<CustomView>
				<InstantSearch
					appId={Config.ALGOLIA_APP_ID}
					apiKey={Config.ALGOLIA_API_KEY}
					indexName="recipes"
				>
					<ConnectedSearchBar />
					<ConnectedHits navigation={this.props.navigation} />
				</InstantSearch>
			</CustomView>
		);
	}
}
