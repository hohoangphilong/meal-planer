import React, { Component } from "react";
import { View, Text } from "react-native";
import { styles } from "./styles";
import firebase from "react-native-firebase";

export default class index extends Component {
	state = {};
	componentDidMount() {
		console.log(
			"============================TEST================================"
		);
		let start = new Date().getTime();
		firebase
			.database()
			.ref("/recipes")
			.limitToFirst(10)
			.once("value")
			.then(res => {
				let end = new Date().getTime();

				console.log("fetch time", end - start);

				console.log("====================================");
				console.log(res.val());
				console.log("====================================");
			})
			.catch(error => {
				console.log("=========error", error);
			});
	}
	render() {
		return (
			<View style={styles.container}>
				<Text>This is test screens</Text>
			</View>
		);
	}
}
