import React, { Component } from "react";
import { View, Text, ScrollView, StatusBar, Image } from "react-native";
import { styles } from "./styles";
import CustomView from "../../components/custom-view";
import Video from "react-native-video";
import backgroundVideo from "../../assets/video.mp4";
import logo from "../../assets/images/logo.png";
import { Button } from "react-native-elements";
import { Router } from "../../navigation/route";

export default class index extends Component {
	state = {};
	render() {
		return (
			<View style={styles.container}>
				<StatusBar hidden={true} />
				<Video
					style={styles.backgroundVideo}
					source={backgroundVideo}
					muted={true}
					repeat={true}
					resizeMode={"cover"}
					rate={1.0}
					ignoreSilentSwitch={"obey"}
					// onLoad={() => {
					// 	alert("loaded");
					// }}
				/>
				<View style={styles.wrapper}>
					<Image
						source={logo}
						resizeMethod={"resize"}
						resizeMode={"contain"}
						style={styles.logo}
					/>
					<Text style={styles.title}>Plan the best meals</Text>
					<Text style={styles.description}>
						For you family right here, right now
					</Text>
					<View style={styles.buttonWrapper}>
						<Button
							title={"Create Account"}
							titleStyle={styles.createTitle}
							buttonStyle={styles.createButton}
							onPress={() => this.props.navigation.navigate(Router.Register)}
						/>
						<Button
							type={"outline"}
							title={"Login"}
							titleStyle={styles.loginTitle}
							buttonStyle={styles.loginButton}
							onPress={() => this.props.navigation.navigate(Router.Login)}
						/>
					</View>
				</View>
			</View>
		);
	}
}
