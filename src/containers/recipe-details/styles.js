import { ScaledSheet } from "react-native-size-matters";
import { StyleSheet } from "react-native";
import { Color, Font } from "../../common";

const headerHeight = 56;

export const styles = ScaledSheet.create({
	container: {
		paddingTop: "300@vs"
	},
	content: {
		backgroundColor: Color.lightGrey,
		paddingHorizontal: "8@ms",
		borderRadius: "8@ms"
	},
	row: {
		flexDirection: "row",
		alignItems: "center"
	},
	quantity: {
		borderWidth: StyleSheet.hairlineWidth,
		borderRadius: 4,
		paddingHorizontal: 8,
		borderColor: Color.green,
		fontSize: 16,
		flex: 1,
		textAlign: "center"
	},
	unit: {
		paddingHorizontal: 16,
		fontFamily: Font.Family.OpenSansLightItalic,
		fontSize: 14,
		flex: 2
	},
	ingredient: {
		flex: 6,
		marginBottom: 8
	},
	image: {
		width: "100%",
		height: "280@vs",
		...StyleSheet.absoluteFillObject
	},
	title: {
		fontSize: 30,
		fontFamily: Font.Family.OpenSansExtraBold,
		color: "black"
	},
	sub: { fontSize: 12, fontFamily: Font.Family.OpenSansLight },
	description: {
		fontSize: 14,
		fontFamily: Font.Family.OpenSansLightItalic
	},
	add: {
		fontFamily: Font.Family.OpenSansLightItalic,
		color: Color.green
	},
	tag: {
		backgroundColor: Color.lightGreen,
		color: "#fafafa",
		fontFamily: Font.Family.OpenSansLightItalic,
		paddingHorizontal: "4@ms",
		borderRadius: 4,
		marginRight: 4
	}
});
