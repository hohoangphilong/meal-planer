import React, { Component } from "react";
import { View, Text, Image, FlatList, TouchableOpacity } from "react-native";
import { styles } from "./styles";
import { titleStyle } from "../../common/global";
import { connect } from "react-redux";
import { getEntireRecipeState, getRecipeField } from "../../selector/recipe";
import ViewWithHeader from "../../components/view-with-header";
import CustomHeader from "../../components/custom-header";
import { getKey } from "../../common/util";
import { Font } from "../../common";
import { Router } from "../../navigation/route";

const cate = [
  "Beef",
  "Chicken",
  "Dessert",
  "Lamb",
  "Miscellaneous",
  "Pasta",
  "Pork",
  "Seafood",
  "Side",
  "Starter",
  "Vegan",
  "Vegetarian",
  "Breakfast",
  "Goat",
  "Italian",
  "Indian",
  "Turkish",
  "Mexican",
  "Thai",
  "Greek",
  "Chinese",
  "Japanese",
  "French",
  "Spanish"
];

class index extends Component {
  static navigationOptions = {
    header: null
  };
  renderIngredient = ({ item }) => (
    <View style={styles.row}>
      <Text style={styles.quantity}>
        {Math.floor(parseFloat(item.quantity || 0).toFixed(2))}
      </Text>
      <Text style={styles.unit}>{item.unit || "---"}</Text>
      <Text style={styles.ingredient}>{item.input}</Text>
    </View>
  );
  addToList = ingredients => () => {
    this.props.navigation.replace(Router.AddToShop, { ingredients });
  };
  render() {
    const {
      navigation: { getParam }
    } = this.props;
    const item = getParam("item");
    const {
      cookTime,
      description,
      image,
      ingredients,
      name,
      prepTime,
      recipeYield,
      instructions
    } = item;
    let tags = getKey(item, true);
    console.log(tags);
    tags = tags.filter(e => {
      if (cate.includes(e)) {
        return e;
      }
    });
    console.log(tags);

    const directions = instructions.split("\r\n");
    return (
      <View style={{ flex: 1 }}>
        <Image
          source={{ uri: image }}
          resizeMethod={"resize"}
          resizeMode={"cover"}
          style={styles.image}
          defaultSource={require("../../assets/images/diet.png")}
        />
        <ViewWithHeader
          contentContainerStyle={styles.container}
          showsVerticalScrollIndicator={false}
          header={
            <CustomHeader navigation={this.props.navigation} title={name} />
          }
        >
          <View style={styles.content}>
            <Text style={styles.title}>{name}</Text>
            <View style={[styles.row, { flexWrap: "wrap", marginTop: 12 }]}>
              {tags.map((e, i) => (
                <Text key={i} style={styles.tag}>
                  {e}
                </Text>
              ))}
            </View>
            <View style={styles.row}>
              <View style={{ flex: 1 }}>
                <Text style={titleStyle}>Servings</Text>
                <Text style={styles.sub}>{recipeYield || 1}</Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={titleStyle}>Cooking time</Text>
                <Text style={styles.sub}>{cookTime}</Text>
              </View>
            </View>

            <Text style={titleStyle}>Description</Text>
            <Text style={styles.description}>{description}</Text>

            <View style={styles.row}>
              <Text style={[titleStyle, { flex: 1 }]}>Ingredients</Text>
              <TouchableOpacity onPress={this.addToList(ingredients)}>
                <Text style={styles.add}>Add to shopping list</Text>
              </TouchableOpacity>
            </View>

            <FlatList
              data={ingredients}
              keyExtractor={(e, i) => i.toString()}
              renderItem={this.renderIngredient}
            />

            <Text style={titleStyle}>Instructions</Text>
            {directions.map((e, i) => (
              <Text
                key={"di" + i.toString()}
                style={[styles.description, { marginBottom: 8 }]}
              >
                <Text style={{ fontFamily: Font.Family.OpenSansRegular }}>
                  {i.toString()}
                </Text>{" "}
                - {e}
              </Text>
            ))}
          </View>
        </ViewWithHeader>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user
});
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(index);
