import React, { Component } from "react";
import {
	View,
	Text,
	ImageBackground,
	ScrollView,
	TouchableOpacity,
	Image
} from "react-native";
import { styles } from "./styles";
import CustomInput from "../../components/custom-input";
import background from "../../assets/images/background.jpeg";
import { Router } from "../../navigation/route";
import { Button, Icon } from "react-native-elements";
import { Color } from "../../common";
import { connect } from "react-redux";
import AuthAction from "../../actions/auth";
import { getField } from "../../selector/auth";
import logo from "../../assets/images/logo.png";
import Snackbar from "react-native-snackbar";
import { setUser } from "../../config/storage";

class index extends Component {
	state = {};
	register = () => {
		// const { register, isFetching, error, navigation } = this.props;
		// const rand = Math.floor(Math.random() * 100);
		// const email = `test${rand}@gmail.com`;
		// const password = "123456";
		// register(email, password);
		// if (isFetching === false && !error) {
		// 	navigation.navigate(Router.Main);
		// }

		const {
			username,
			email,
			password,
			emailError,
			passwordError,
			confirmError,
			isChecked,
			register,
			navigation
		} = this.props;
		if (
			emailError === "" &&
			passwordError === "" &&
			confirmError === "" &&
			isChecked &&
			username !== ""
		) {
			register(email, password, username);
		}
	};
	async componentWillReceiveProps(nextProps) {
		if (nextProps.logoutState === false && nextProps.user) {
			Snackbar.show({
				title: "Register Successfully"
			});
			await setUser(nextProps.user);
			this.props.navigation.navigate(Router.Main);
		}
	}
	render() {
		const {
			isFetching,
			username,
			password,
			passwordError,
			email,
			emailError,
			confirm,
			confirmError,
			isChecked,
			changeFormField,
			checkEmail,
			checkConfirm,
			checkPassword
		} = this.props;
		return (
			<ImageBackground
				source={background}
				style={styles.container}
				blurRadius={0.5}
			>
				<ScrollView showsVerticalScrollIndicator={false}>
					<Image
						source={logo}
						resizeMethod={"resize"}
						resizeMode={"contain"}
						style={styles.logo}
					/>
					<View style={styles.form}>
						<Text style={styles.title}>Please fill the information below</Text>
						<CustomInput
							label={"Username"}
							value={username}
							onChangeText={username => changeFormField("username", username)}
						/>
						<CustomInput
							label={"Email"}
							value={email}
							onChangeText={email => {
								checkEmail(email);
								changeFormField("email", email);
							}}
							errorText={emailError}
							keyboardType={"email-address"}
						/>
						<CustomInput
							label={"Password"}
							value={password}
							onChangeText={password => {
								checkPassword(password);
								changeFormField("password", password);
							}}
							errorText={passwordError}
							secureTextEntry={true}
						/>
						<CustomInput
							label={"Confirm password"}
							value={confirm}
							onChangeText={confirm => {
								checkConfirm(password, confirm);
								changeFormField("confirm", confirm);
							}}
							errorText={confirmError}
							secureTextEntry={true}
						/>
						<TouchableOpacity
							onPress={this.props.toggleCheck}
							style={styles.insWrapper}
						>
							<Icon
								name={isChecked ? "checkcircle" : "checkcircleo"}
								type={"antdesign"}
								color={!isChecked ? "lightgrey" : Color.lightGreen}
								size={20}
							/>
							<Text style={styles.instruction}>
								I accept
								<Text style={styles.highlight}> terms and conditions</Text>
							</Text>
						</TouchableOpacity>
					</View>
					<Button
						title={"register"}
						titleStyle={styles.createTitle}
						buttonStyle={styles.createButton}
						containerStyle={{ alignSelf: "center" }}
						onPress={this.register}
						loading={isFetching}
					/>
				</ScrollView>
			</ImageBackground>
		);
	}
}

const mapStateToProps = state => ({
	isFetching: state.auth.isFetching,
	error: state.auth.error,
	user: state.auth.user,
	email: getField(state, "email"),
	emailError: getField(state, "emailError"),
	password: getField(state, "password"),
	passwordError: getField(state, "passwordError"),
	username: getField(state, "username"),
	confirm: getField(state, "confirm"),
	confirmError: getField(state, "confirmError"),
	isChecked: getField(state, "isChecked"),
	logoutState: state.auth.logoutState
});

const mapDispatchToProps = {
	register: AuthAction.register,
	changeFormField: AuthAction.changeFormField,
	toggleCheck: AuthAction.toggleCheck,
	checkEmail: AuthAction.checkEmail,
	checkPassword: AuthAction.checkPassword,
	checkConfirm: AuthAction.checkConfirm
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
