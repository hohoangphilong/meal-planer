import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { styles } from "./styles";
import ViewWithHeader from "../../components/view-with-header";
import CustomHeader from "../../components/custom-header";
import { titleStyle } from "../../common/global";
import ShoppingItem from "./components/shopping-item";
import { connect } from "react-redux";
import ShopAction from "../../actions/shop";
import { getShopField } from "../../selector/shop";
import Loader from "../../components/loader";
import { Icon } from "react-native-elements";
import { Router } from "../../navigation/route";

let temp = { ingredients: [], name: "", note: "", id: "" };
const tempIng = [
  "carrots",
  "herm",
  "roses",
  "cheese",
  "butter",
  "chicken leg",
  "chicken wing",
  "pepper",
  "chilli pepper",
  "crab",
  "shrimp",
  "slice of salmon",
  "fish oil"
];

class index extends Component {
  static navigationOptions = {
    header: null
  };
  data = [];
  componentDidMount() {
    const {
      getList,
      navigation: { getParam }
    } = this.props;
    getList(this.props.user.uid);
    this.data = getParam("ingredients").map((e, i) => ({
      id: i,
      quantity: e.quantity || 0,
      ingredient: e.name.split(" ").pop(),
      checked: false
    }));
  }
  state = {};
  renderListHeader = () => {
    return (
      <View>
        <TouchableOpacity
          style={[styles.row, { justifyContent: "center" }]}
          onPress={() => this.onAddList("create", temp)}
        >
          <Icon
            name={"plus-square-o"}
            type={"font-awesome"}
            size={24}
            color={"#eee"}
          />
          <Text style={styles.newText}>New Shopping List</Text>
        </TouchableOpacity>
        <View style={[styles.row, { marginBottom: 12 }]}>
          <View style={styles.line} />
          <Text style={styles.split}>OR</Text>
          <View style={styles.line} />
        </View>
        <Loader visible={this.props.isLoading} />
      </View>
    );
  };
  renderItem = ({ item }) => (
    <ShoppingItem data={item} onPress={() => this.onAddList("update", item)} />
  );
  onAddList = (type, { ingredients, name, note, id }) => {
    const {
      changeFormField,
      navigation: { replace, getParam }
    } = this.props;
    changeFormField("id", id);
    changeFormField("name", name);
    changeFormField("note", note);

    const length = ingredients.length;
    this.data = getParam("ingredients").map((e, i) => {
      let rand = Math.floor(Math.random() * (tempIng.length - 1));
      return {
        id: i + length,
        quantity: e.quantity || 0,
        ingredient: e.name === "" ? tempIng[rand] : e.name,
        checked: false
      };
    });
    changeFormField("ingredients", [...ingredients, ...this.data]);
    changeFormField("currentId", length + this.data.length - 1);

    replace(Router.AddShopList, { type });
  };
  render() {
    const { data } = this.props;
    return (
      <ViewWithHeader
        contentContainerStyle={styles.container}
        showsVerticalScrollIndicator={false}
        header={
          <CustomHeader
            navigation={this.props.navigation}
            title={"Choose Shopping List"}
          />
        }
        data={data}
        extraData={data}
        keyExtractor={item => item.id.toString()}
        renderItem={this.renderItem}
        ListHeaderComponent={this.renderListHeader}
      />
    );
  }
}

const mapStateToProps = state => ({
  form: getShopField(state, "form"),
  isLoading: getShopField(state, "isLoading"),
  error: getShopField(state, "error"),
  data: getShopField(state, "data"),
  user: state.auth.user
});

const mapDispatchToProps = {
  changeFormField: ShopAction.changeFormField,
  getList: ShopAction.getList
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
