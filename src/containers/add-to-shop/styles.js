import { ScaledSheet } from "react-native-size-matters";
import { Font } from "../../common";
import { StyleSheet } from "react-native";

export const styles = ScaledSheet.create({
	container: { padding: "8@ms", paddingTop: 56 },
	row: { flexDirection: "row", alignItems: "center" },
	newText: {
		fontSize: Font.Size.header,
		letterSpacing: 1,
		margin: "8@ms"
	},
	split: {
		fontSize: 16,
		fontFamily: Font.Family.OpenSansBold
	},
	line: {
		height: StyleSheet.hairlineWidth,
		backgroundColor: "#ccc",
		flex: 1,
		marginHorizontal: "16@ms"
	}
});
