import { ScaledSheet } from "react-native-size-matters";
import { StyleSheet } from "react-native";
import { Font, Color } from "../../common";

export const styles = ScaledSheet.create({
  container: { flex: 1 },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  name: {
    paddingHorizontal: "8@ms",
    flex: 1
  },
  section: {
    paddingHorizontal: "8@ms",
    marginBottom: "8@vs"
  },
  sectionHeader: {
    borderTopWidth: StyleSheet.hairlineWidth,
    borderColor: "#999"
  },
  headerTitle: {
    textTransform: "uppercase",
    flex: 1,
    fontFamily: Font.Family.OpenSansSemiBoldItalic
  },
  add: {
    padding: "8@ms"
  },
  footer: {
    backgroundColor: "#fff",
    elevation: 4
  },
  remove: {
    paddingHorizontal: "8@ms"
  },
  addIng: {
    fontFamily: Font.Family.OpenSansLightItalic,
    color: Color.green,
    paddingVertical: "8@vs",
    paddingHorizontal: "8@ms"
  }
});
