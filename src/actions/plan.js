import { createAction, convertDataToArray } from "../common/util";
import firebase from "react-native-firebase";
import uuid from "uuid/v4";
import _ from "lodash";

export const Types = {
	getPlansRequest: "get-plans-request",
	getPlansSuccess: "get-plans-success",

	changeFormField: "change-form-field",

	pushItem: "push-item",
	removeItem: "remove-item",

	setForm: "set-form"
};

const changeFormField = (key, value) => dispatch => {
	console.log("change form field", key, value);
	dispatch(createAction(Types.changeFormField, { key, value }));
};

const setForm = (formData = {}) => dispatch => {
	// console.log("set form", formData);
	dispatch(createAction(Types.setForm, { formData }));
};

const resetForm = () => (dispatch, getState) => {
	const {
		user: { uid }
	} = getState().auth;
	let temp = {
		uuid: uuid(),
		ref: uid,
		dateString: "",
		breakfast: [],
		lunch: [],
		dinner: []
	};
	console.log("reset form");
	dispatch(setForm(temp));
};

const pushItem = (recipe, key) => (dispatch, getState) => {
	console.log("push item to", key, recipe.name);
	dispatch(createAction(Types.pushItem, { recipe, key }));
};

const removeItem = (recipe, key) => (dispatch, getState) => {
	console.log("remove item", recipe.name, key);
	dispatch(createAction(Types.removeItem, { recipe, key }));
};

const defaultPlan = {
	uuid: "",
	ref: "",
	dateString: "",
	breakfast: [],
	lunch: [],
	dinner: []
};

const getPlans = uid => (dispatch, getState) => {
	console.log("get plans");
	dispatch(createAction(Types.getPlansRequest));
	return firebase
		.database()
		.ref("plans/")
		.orderByChild("ref")
		.equalTo(uid)
		.once("value")
		.then(res => {
			let data = [];
			if (res.val() !== null) {
				data = Object.values(res.val()).map(e => {
					console.log("***********", e.dateString);

					return { ...defaultPlan, ...e };
				});
				// data.map(e => {
				// 	console.log(e.dateString);
				// });
			}
			console.log("get plans success", res.val());
			dispatch(createAction(Types.getPlansSuccess, { data }));
		});
};

const setPlan = () => (dispatch, getState) => {
	const { form } = getState().plan;
	console.log("set plans", form);
	return firebase
		.database()
		.ref(`plans/${form.uuid}`)
		.set(form); //return Promise <nullable Error>
};

const removePlan = uuid => (dispatch, getState) => {
	console.log("remove plans", uuid);
	return firebase
		.database()
		.ref(`plans/${uuid}`)
		.remove(); //return Promise <nullable Error>
};

export default {
	changeFormField,
	getPlans,
	setPlan,
	removePlan,
	pushItem,
	removeItem,
	setForm,
	resetForm
};
