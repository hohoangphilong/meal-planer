import React from "react";
import { Text } from "react-native";
import { Font, Color } from ".";
import { verticalScale, moderateScale } from "react-native-size-matters";

export const customViewProps = {
	style: {}
};

export const customTextInputProps = {
	defaultProps: {
		allowFontScaling: false,
		clearButtonMode: "while-editing"
	},
	style: {
		fontSize: Font.Size.header,
		color: "#333",
		fontFamily: Font.Family.OpenSansRegular,
		padding: verticalScale(8),
		paddingHorizontal: moderateScale(16),
		backgroundColor: "#eee",
		borderRadius: moderateScale(8)
	}
};

export const customTextProps = {
	defaultProps: {
		allowFontScaling: false
	},
	style: {
		fontFamily: Font.Family.OpenSansLight,
		fontSize: Font.Size.body1,
		color: "grey"
	}
};

export const titleStyle = {
	fontSize: Font.Size.body2,
	fontFamily: Font.Family.OpenSansSemiBold,
	color: "grey",
	marginVertical: verticalScale(16)
};

export const inputStyle = {
	fontSize: Font.Size.header,
	fontFamily: Font.Family.OpenSansLight,
	color: "#333",
	backgroundColor: "#eee",
	borderRadius: verticalScale(8),
	padding: verticalScale(4),
	paddingHorizontal: moderateScale(8)
};

export const optionsStyles = {
	optionsContainer: {
		width: moderateScale(350),
		padding: verticalScale(8),
		paddingHorizontal: moderateScale(16),
		margin: 0
	},
	optionsWrapper: {
		margin: 10
	}
};

export const headerTitleRight = {
	fontSize: Font.Size.header,
	margin: moderateScale(16),
	color: Color.green
};

export const renderHeader = title => ({
	headerTitle: (
		<Text
			style={{
				fontSize: Font.Size.h6
			}}
		>
			{title}
		</Text>
	)
});

// export const topTabProps = {
// 	activeTintColor: Color.lightViolet,
// 	inactiveTintColor: "#fff",
// 	style: {
// 		backgroundColor: Color.primaryDark
// 	},
// 	indicatorStyle: {
// 		backgroundColor: Color.violet
// 	},
// 	labelStyle: {
// 		fontSize: Font.Size.body1
// 	},
// 	upperCaseLabel: false
// };
