export const createAction = (type, payload = {}) => ({ type, payload });

export function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export function validatePassword(password) {
  return password && password.length >= 6 && password.length < 30;
}

export function validateName(name) {
  return name && name.length >= 6 && name.length < 30;
}

export function validatePhone(value) {
  const re = /^[0-9]{9}[0-9]$/g;
  return re.test(value);
}

export const convertTimeToString = (time, format) => {
  const date = new Date(time);
  const year = date.getFullYear();
  const mon = ("0" + (date.getMonth() + 1)).slice(-2);
  const day = ("0" + date.getDate()).slice(-2);
  const hour = ("0" + date.getHours()).slice(-2);
  const min = ("0" + date.getMinutes()).slice(-2);
  const second = ("0" + date.getSeconds()).slice(-2);

  return format
    .replace("YYYY", year)
    .replace("yyyy", year)
    .replace("dd", day)
    .replace("DD", day)
    .replace("MM", mon)
    .replace("hh", hour)
    .replace("mm", min)
    .replace("ss", second);
};

export function getHumanReadableNumber(number, fixed = 2) {
  if (!number) {
    return 0;
  }
  const postfix = ["K", "M", "B", "T"];
  const arrange = [1000, 1000000, 1000000000, 1000000000000];
  for (let i = arrange.length - 1; i >= 0; i--) {
    if (number > arrange[i]) {
      number = (number / arrange[i]).toFixed(fixed) + postfix[i];
      break;
    }
  }

  return number;
}

export const formatSeparator = num => {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

export const formatNumber = currency => {
  return parseInt(currency.replace(/,/g, ""));
};

export const convertDataToArray = data => {
  if (!Array.isArray(data)) {
    const keys = Object.keys(data);
    const values = Object.values(data);

    values.map((item, index) => (item.id = keys[index]));

    return values;
  }

  return data.map((e, i) => {
    return (e.id = i);
  });
};

export const getKey = (obj, val) =>
  Object.keys(obj).filter(e => obj[e] === val);
