export default {
	green: "#1B8C42",
	lightGreen: "#30BF62",
	orange: "#F26A1B",
	lightGrey: "#fafafa",
	primaryDark: "#0D0D0D",
	red: "#F26671"
};
