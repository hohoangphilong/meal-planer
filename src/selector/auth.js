import { createSelector } from "reselect";

export const getEntireState = state => state.auth;

export const getField = (state, field) => state.auth.form[field];
