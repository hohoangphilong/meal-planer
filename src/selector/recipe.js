import { createSelector } from "reselect";

export const getEntireRecipeState = state => state.recipe;

export const getRecipeField = (state, field) => state.recipe[field];
