import { createSelector } from "reselect";

export const getEntireShopState = state => state.shop;

export const getShopField = (state, field) => state.shop[field];
