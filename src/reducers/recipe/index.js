import { Types } from "../../actions/recipe";
import initialState from "./initial-state";
import _ from "lodash";

export default (state = initialState, action) => {
	switch (action.type) {
		case Types.toggleUserLike:
			let newState = _.cloneDeep(state);
			const { status, uid, index } = action.payload;
			newState.data[index][uid] = status;
			return newState;

		case Types.changeCategory:
			return state.set("category", action.payload.category);

		case Types.getRecipeRequest:
			return state.set("isLoading", true).set("error", null);
		// case Types.getRecipeSuccess:
		// 	return state
		// 		.set("isLoading", false)
		// 		.set("error", null)
		// 		.set(
		// 			"data",
		// 			action.payload.reset
		// 				? action.payload.data
		// 				: state.data.concat(action.payload.data)
		// 		);
		case Types.getRecipeSuccess:
			return state
				.set("isLoading", false)
				.set("error", null)
				.set("data", state.data.concat(action.payload.data));
		case Types.getRecipeFailure:
			return state.set("isLoading", false).set("error", action.payload.error);
		default:
			return state;
	}
};
