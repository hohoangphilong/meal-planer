import { Types } from "../../actions/plan";
import initialState from "./initial-state";

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.changeFormField:
      return state.setIn(["form", action.payload.key], action.payload.value);

    case Types.setForm:
      return state.set("form", action.payload.formData);

    case Types.pushItem:
      let exist = state.form[action.payload.key].findIndex(
        e => e.uuid === action.payload.recipe.uuid
      );
      let tempArray =
        exist !== -1
          ? [...state.form[action.payload.key]]
          : [action.payload.recipe, ...state.form[action.payload.key]];
      return state.setIn(["form", action.payload.key], tempArray);

    case Types.removeItem:
      let temp = [...state.form[action.payload.key]];
      temp.splice(
        temp.findIndex(e => e === action.payload.recipe),
        1
      );
      return state.setIn(["form", action.payload.key], temp);

    case Types.getPlansRequest:
      return state.set("isLoading", true);
    case Types.getPlansSuccess:
      return state.set("isLoading", false).set("data", action.payload.data);

    default:
      return state;
  }
};
