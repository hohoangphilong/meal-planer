import { Record } from "immutable";

export default new Record({
	isLoading: false,
	data: [
		// {
		// 	uuid: "000",
		// 	ref: "000", //user id
		// 	dateString: "2019-12-22",
		// 	breakfast: [
		// 		{
		// 			...recipe
		// 		}
		// 	],
		// 	lunch: [
		// 		{
		// 			...recipe
		// 		}
		// 	],
		// 	dinner: [
		// 		{
		// 			...recipe
		// 		}
		// 	]
		// }
	],
	form: new Record({
		uuid: "",
		ref: "",
		dateString: "",
		breakfast: [],
		lunch: [],
		dinner: []
	})()
})();
