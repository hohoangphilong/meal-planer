import { Record } from "immutable";

export default new Record({
	isLoading: false,
	error: null,
	data: []
})();
