import { combineReducers } from "redux";

import auth from "./auth";
import recipe from "./recipe";
import category from "./category";
import shop from "./shop";
import plan from "./plan";

export default combineReducers({
	auth,
	recipe,
	category,
	shop,
	plan
});
