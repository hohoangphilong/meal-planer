import { Types } from "../../actions/shop";
import initialState from "./initial-state";

export default (state = initialState, action) => {
	switch (action.type) {
		case Types.changeFormField:
			const { key, value } = action.payload;
			return state.setIn(["form", key], value);
		case Types.resetForm:
			return state.set("form", initialState.form);
		case Types.actionRequest:
			return state
				.setIn(["form", "isLoading"], true)
				.setIn(["form", "error"], null);
		case Types.actionSuccess:
			return state
				.setIn(["form", "isLoading"], false)
				.setIn(["form", "error"], null);
		case Types.actionFailure:
			return state
				.setIn(["form", "isLoading"], false)
				.setIn(["form", "error"], action.payload.error);
		case Types.getListRequest:
			return state.set("isLoading", true).set("error", null);
		case Types.getListSuccess:
			return state
				.set("isLoading", false)
				.set("error", null)
				.set("data", action.payload.data);
		case Types.getListFailure:
			return state
				.set("isLoading", false)
				.set("error", action.payload.error)
				.set("data", []);
		default:
			return state;
	}
};
