import React from "react";
import { Animated, Text } from "react-native";
import PropTypes from "prop-types";
import NumberFormat from "react-number-format";

export default class AnimatedNumber extends React.PureComponent {
	static propTypes = {
		type: PropTypes.string,
		value: PropTypes.number,
		initialValue: PropTypes.number
	};
	static defaultProps = {
		type: "timing",
		value: 0,
		initialValue: null
	};
	constructor(props) {
		super(props);
		const { initialValue } = props;
		const firstValue = initialValue !== null ? initialValue : props.value;
		const animatedValue = new Animated.Value(firstValue);
		animatedValue.addListener(this.onValueChanged);

		this.state = {
			animatedValue,
			value: firstValue
		};
	}
	componentWillReceiveProps(nextProps) {
		const { value } = this.props;

		if (value !== nextProps.value) {
			this.move(nextProps);
		}
	}
	onValueChanged = e => {
		this.setState({
			value: Math.round(e.value)
		});
	};
	move = props => {
		const { value, style, type, ...rest } = props;
		const { animatedValue } = this.state;

		Animated[type](animatedValue, {
			toValue: value,
			useNativeDriver: true,
			...rest
		}).start();
	};
	render() {
		const { value } = this.state;

		return (
			<NumberFormat
				value={value}
				displayType={"text"}
				thousandSeparator={","}
				decimalSeparator={"."}
				renderText={value => <Animated.Text>{value}</Animated.Text>}
			/>
		);
	}
}
