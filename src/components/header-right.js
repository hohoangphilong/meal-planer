import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";
import { Color } from "../common";

export default ({ title, onPress }) => (
  <TouchableOpacity style={styles.container} onPress={onPress}>
    <Text style={styles.btn}>{title}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center"
  },
  btn: {
    fontSize: 16,
    padding: 8,
    paddingHorizontal: 12,
    backgroundColor: Color.coral,
    borderRadius: 4,
    color: "#fff"
  }
});
