import React, { Component, createRef } from "react";
import PropTypes from "prop-types";
import { TextInput, StyleSheet, Animated, View, Text } from "react-native";
import { Color, Font } from "../common";

export default class CustomInput extends Component {
	constructor(props) {
		super(props);
		this.input = createRef();
		this.focus = this.focus.bind(this);
		this.state = { isFocused: false };
	}

	static propTypes = {
		fontSize: PropTypes.number,
		label: PropTypes.string,
		leftIcon: PropTypes.element,
		rightElement: PropTypes.element,
		errorText: PropTypes.string,
		textColor: PropTypes.string,
		lineColor: PropTypes.string,
		errorTextColor: PropTypes.string,
		backgroundColor: PropTypes.string
	};
	static defaultProps = {
		fontSize: 16,
		label: "Label",
		errorText: "",
		textColor: "#fff",
		lineColor: "#f4f4f4",
		errorTextColor: Color.red,
		backgroundColor: "transparent"
	};
	inputRef() {
		return this.input.current;
	}
	focus() {
		if (this.state.isFocused !== false) {
			this.inputRef().focus();
		}
	}

	componentWillMount() {
		this._animatedIsFocused = new Animated.Value(
			this.props.value === "" ? 0 : 1
		);
	}
	componentDidUpdate() {
		Animated.timing(this._animatedIsFocused, {
			toValue: this.state.isFocused || this.props.value !== "" ? 1 : 0,
			duration: 200,
			useNativeDriver: true
		}).start();
	}
	_labelTranslateX = 0;
	calculateScale = ({ nativeEvent }) => {
		this._labelTranslateX = Math.round(
			(nativeEvent.layout.width * (1 - 0.8)) / 2
		);
	};

	handleFocus = () => this.setState({ isFocused: true });
	handleBlur = () => this.setState({ isFocused: false });

	render() {
		const {
			label,
			fontSize,
			leftIcon,
			rightElement,
			errorText,
			backgroundColor,
			textColor,
			errorTextColor,
			lineColor,
			...props
		} = this.props;
		const labelTranslateY = this._animatedIsFocused.interpolate({
				inputRange: [0, 1],
				outputRange: [0, -fontSize * 1.1]
			}),
			labelTranslateX = this._animatedIsFocused.interpolate({
				inputRange: [0, 1],
				outputRange: [0, -this._labelTranslateX]
			}),
			labelSize = this._animatedIsFocused.interpolate({
				inputRange: [0, 1],
				outputRange: [1, 0.8]
			}),
			labelColor = this._animatedIsFocused.interpolate({
				inputRange: [0, 1],
				outputRange: [0.7, 1]
			}),
			underlineHeight = this._animatedIsFocused.interpolate({
				inputRange: [0, 1],
				outputRange: [1, 3]
			}),
			underlineColor = this._animatedIsFocused.interpolate({
				inputRange: [0, 1],
				outputRange: [1, 0]
			});
		return (
			<View>
				<View
					style={StyleSheet.flatten([styles.container, { backgroundColor }])}
				>
					{leftIcon && <View style={styles.leftIcon}>{leftIcon}</View>}
					<View style={{ paddingTop: fontSize, flex: 1 }}>
						<Animated.Text
							onLayout={this.calculateScale}
							style={{
								position: "absolute",
								top: fontSize * 1.1,
								left: 0,
								opacity: labelColor,
								color: textColor,
								fontSize: fontSize,
								transform: [
									{ translateY: labelTranslateY },
									{ translateX: labelTranslateX },
									{ scale: labelSize }
								]
							}}
						>
							{label}
						</Animated.Text>
						<TextInput
							ref={this.input}
							{...props}
							style={{
								fontSize: fontSize,
								color: textColor,
								paddingVertical: 2
							}}
							onFocus={this.handleFocus}
							onBlur={this.handleBlur}
						/>
					</View>

					<Animated.View
						style={{
							position: "absolute",
							bottom: 0,
							left: 0,
							right: 0,
							height: StyleSheet.hairlineWidth,
							backgroundColor: lineColor,
							transform: [{ scaleY: underlineHeight }]
						}}
					>
						<Animated.View
							style={{
								opacity: underlineColor,
								...StyleSheet.absoluteFill,
								backgroundColor: "#ccc"
							}}
						/>
					</Animated.View>
					{/* for show/hide password icon, get max amount button, etc. */}
					{rightElement && (
						<View style={styles.rightElement}>{rightElement}</View>
					)}
				</View>
				{errorText !== "" && (
					<Text
						style={{
							marginTop: 4,
							marginLeft: 16,
							fontSize: Font.Size.caption,
							fontStyle: "italic",
							color: errorTextColor
						}}
					>
						{errorText}
					</Text>
				)}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flexDirection: "row",
		alignItems: "flex-end",
		paddingHorizontal: 8
	},
	leftIcon: { marginRight: 8, marginBottom: 8 },
	rightElement: { marginLeft: 8, marginBottom: 4 }
});
