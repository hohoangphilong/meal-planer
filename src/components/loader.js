import React from "react";
import { ActivityIndicator, Modal, View } from "react-native";
import PropTypes from "prop-types";
import { ScaledSheet } from "react-native-size-matters";

export class LoadingModal extends React.Component {
	render() {
		return (
			<Modal visible={this.props.visible || false} transparent={true}>
				<View style={styles.container}>
					<ActivityIndicator size="large" />
				</View>
			</Modal>
		);
	}
}
LoadingModal.propsType = {
	visible: PropTypes.bool
};

const styles = ScaledSheet.create({
	container: {
		flex: 1,
		backgroundColor: "rgba(0,0,0,.1)",
		alignItems: "center",
		justifyContent: "center"
	}
});

export default LoadingModal;
