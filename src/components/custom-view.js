import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, StyleSheet } from "react-native";
import { ScaledSheet } from "react-native-size-matters";
import { Color } from "../common";

export default class CustomView extends Component {
	static propTypes = {
		containerStyle: PropTypes.object,
		transparent: PropTypes.bool
	};
	render() {
		const { children, containerStyle, transparent, ...rest } = this.props;
		return (
			<View
				style={StyleSheet.flatten([
					styles.container,
					!transparent && { backgroundColor: Color.lightGrey }
				])}
			>
				<View
					style={StyleSheet.flatten([styles.wrapper, containerStyle])}
					{...rest}
				>
					{children}
				</View>
			</View>
		);
	}
}

const styles = ScaledSheet.create({
	container: {
		flex: 1,
		alignItems: "center"
	},
	wrapper: {
		width: "350@ms",
		flex: 1
	}
});
