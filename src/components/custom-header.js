import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Platform
} from "react-native";
import { ScaledSheet } from "react-native-size-matters";
import { Icon } from "react-native-elements";
import { Color, Font } from "../common";

const headerHeight = 56;

export default class CustomHeader extends PureComponent {
  render() {
    const {
      navigation,
      headerLeft,
      headerCenter,
      headerRight,
      title
    } = this.props;
    const icon = (
      <Icon
        name={Platform.select({
          ios: "ios-arrow-back",
          android: "md-arrow-back"
        })}
        type={"ionicon"}
        size={24}
        color={"grey"}
        containerStyle={styles.icon}
      />
    );
    return (
      <View style={styles.header}>
        {headerLeft || (
          <TouchableOpacity
            style={styles.btn}
            onPress={() => navigation.goBack()}
          >
            {(title === "" || !title) && !headerCenter ? (
              <Text style={styles.back}>Quay lại</Text>
            ) : (
              icon
            )}
          </TouchableOpacity>
        )}
        {headerCenter || (
          <Text numberOfLines={1} style={styles.title}>
            {title || ""}
          </Text>
        )}
        {headerRight}
      </View>
    );
  }
}

const styles = ScaledSheet.create({
  header: {
    height: headerHeight,
    flexDirection: "row",
    alignItems: "stretch",
    paddingHorizontal: 8
  },
  btn: {
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    flex: 1,
    paddingHorizontal: 16,
    textAlignVertical: "center",
    fontSize: Font.Size.h6,
    fontFamily: Font.Family.OpenSansRegular,
    color: Color.coral
  },
  back: {
    padding: 8,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#ccc",
    borderRadius: 4,
    fontSize: 16
  },
  icon: { marginLeft: 8 }
});
