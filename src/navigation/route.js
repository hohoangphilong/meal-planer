export const Router = {
	Test: "Test",

	Splash: "Splash",
	Intro: "Intro",
	Explore: "Explore",
	Forgot: "Forgot",
	Login: "Login",
	Plan: "Plan",
	Profile: "Profile",
	Register: "Register",
	Shop: "Shop",
	Nav: "Nav",
	Auth: "Auth",
	Main: "Main",
	Search: "Search",
	CreateCollection: "CreateCollection",
	RecipeDetails: "RecipeDetails",
	AddShopList: "AddShopList",
	AddToShop: "AddToShop",
	ListDetail: "ListDetail",
	CookDetail: "CookDetail"
};
