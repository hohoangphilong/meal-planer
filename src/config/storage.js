import Storage from "@react-native-community/async-storage";

const keys = {
	userData: "@storage_user_data"
};

export const getUser = () => {
	return Storage.getItem(keys.userData);
};

export const setUser = data => {
	return Storage.setItem(keys.userData, JSON.stringify(data));
};
