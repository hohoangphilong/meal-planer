/**
 * @format
 */

import { AppRegistry } from "react-native";
import App from "./App";
import { name as appName } from "./app.json";

import {
	setCustomText,
	setCustomView,
	setCustomTextInput
} from "react-native-global-props";
import {
	customTextInputProps,
	customTextProps,
	customViewProps
} from "./src/common/global";

setCustomTextInput(customTextInputProps);
setCustomView(customViewProps);
setCustomText(customTextProps);

AppRegistry.registerComponent(appName, () => App);
